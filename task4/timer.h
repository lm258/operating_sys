#ifndef TIMER_H_
#define TIMER_H_


/*******************************************************************************
 *
 * TIMERwc_time: returns wall clock time in secs and nano secs
 *
 ******************************************************************************/
extern void TIMERwc_time( int *sec, int *nano);

#endif /* TIMER_H_ */
