#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/* This is used to store the state of our threads */
typedef enum { UNDEF , START , FINISHED } STATE;

/* Init our two arrays for adding together */
int arrayA[1000];
int arrayB[1000];
int result[1000];
volatile int count[1000];

/* Define the amount of times we should run our test for an average time */
#define ITR_COUNT 100000

/* This is our number of threads */
int thread_count = 5;
int stagger = 0;
void * work( void * d );

/* Make program , takes one argument which is the number of threads */
int main( int argc , char ** args ){

	int i;

	if ( argc != 2 ){
		fprintf( stderr , "First argument must be number of threads ... < 1000 \n");
		exit(1);
	}

	printf("Initalizing arrays ..... \n");

	/* Init our first array with random numbers */
	for( i = 0 ; i < 1000 ; i ++ ){
		arrayA[i] = i;//rand();
		arrayB[i] = i;//rand();
	}

	thread_count = (atoi(args[1]) % 1000 );

	if ( thread_count == 0 ){
		fprintf( stderr , "Error: Thread count zero!\n");
		exit(2);
	}

	//work out how many elements a thread will work on at any time
	stagger = 1000 / thread_count;
	pthread_t threads[thread_count];

	/* Store our times before and after */
	int time_sec,time_nsec,time_sect,time_nsect;

	/* get our starting time */
	TIMERwc_time ( &time_sec , &time_nsec );

	/* Create all of our threads */
	for ( i = 0 ; i < thread_count ; i++ ){
		pthread_create( &threads[i] , NULL , work , (void *)i );
	}

	/* This is used to destroy our threads */
	for ( i = 0 ; i < thread_count ; i++ ){
		pthread_join( threads[i] , NULL );
	}

	/* get our time after the operation is complete */
	TIMERwc_time ( &time_sect , &time_nsect );

	/* Calculate our total time */
	double total_time = ( time_sect - time_sec )*1000.0 + (double)( time_nsect -time_nsec )/1000000.0;
	total_time = total_time / ITR_COUNT;

	/* Print the total time it took */
	printf("It took %f sec in total.....\n" , total_time );

}

/* This is our working thread , used to calculate our addition concurrently */
void * work ( void * data ){

	/* Get our thread number and our start/end position */
	int thread_num = (int)data;
	int start_pos = thread_num * stagger;
	int end_pos = (start_pos + stagger);

	/* Make sure that we catch all of the array we are working on */
	if ( (end_pos + stagger ) >= 1000 )
		end_pos = 1000;

	int i = 0;

	/* While we are not going to stop */
	while( count[thread_num] < ITR_COUNT ){

		for( i = start_pos ; i < end_pos ; i ++ ){
			result[i] = arrayA[i] + arrayB[i];
		}

		count[thread_num]++;

	}

}
