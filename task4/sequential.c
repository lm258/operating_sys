#include <time.h>
#include <stdio.h>
#include <stdlib.h>

/* Init our two arrays for adding together */
int arrayA[1000];
int arrayB[1000];
int result[1000];

/* Define the amount of times we should run our test for an average time */
#define ITR_COUNT 100000

int main( int argc , char ** args ){

	int i,j;

	/* Init our random number generator */
	srand(time(NULL));

	printf("Initalizing arrays ..... \n");

	/* Init our first array with random numbers */
	for( i = 0 ; i < 1000 ; i ++ ){
		arrayA[i] = rand();
		arrayB[i] = rand();
	}
		

	printf("Starting tests , iterating %d times ..... \n" , ITR_COUNT );

	/* Store our times before and after */
	int time_sec,time_nsec,time_sect,time_nsect;

	/* get our starting time */
	TIMERwc_time ( &time_sec , &time_nsec );

	for ( i = 0 ; i < ITR_COUNT ; i ++ ){

		/* Now do our sequential add */
		for ( j = 0 ; j < 1000 ; j ++ ){
			result[j] =  arrayA[j] + arrayB[j];
		}
	}

	/* get our time after the operation is complete */
	TIMERwc_time ( &time_sect , &time_nsect );

	/* Calculate our total time */
	double total_time = ( time_sect - time_sec )*1000.0 + (double)( time_nsect -time_nsec )/1000000.0;
	total_time = total_time / ITR_COUNT;

	/* Print the total time it took */
	printf("It took %f sec in total.....\n" , total_time );

}