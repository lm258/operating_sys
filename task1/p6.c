#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string>
#include <assert.h>

int main( int argc , char * argv[] ){
	
	//make sure we have a valid number of arguments
	if ( argc < 3  ){
		printf("Invalid parameters supplied!{ %d }\n" , argc);
		return 0;
	}

	std::string command ( "/bin/ls " );

	//generate the correct command based on our arugments
	for( int i = 2 ; i < argc ; i ++ ){
		command += argv[i];
		command += " ";	
	}

	//redirect our standard output
	command += "2>&1";

	//set our command variables
	const char * commandOne = command.c_str();
	const char * commandTwo = argv[1];
	FILE * pipeRead;
	FILE * pipeWrite;

	//reado ur pipes
	pipeRead = popen( commandOne , "r" );
	pipeWrite = popen( commandTwo , "w" );

	//make sure we have a pid
	assert( (pipeRead != NULL) && (pipeWrite != NULL) );

	//now lets read our input from the pipe and write it to the out pipe
	char buffer[256];

	while ( !feof( pipeRead ) ){
		if ( fgets ( buffer , 256 , pipeRead ) == NULL ){
			/*pclose( pipeRead );
			pclose( pipeWrite );
			perror("ERROR Reading from the PIPE!\n");*/
			break;
		}

		fputs ( buffer , pipeWrite );
	}

	pclose ( pipeRead );
	pclose ( pipeWrite );
	
	return 0;
}

