#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main( int argc , char * argv[] ){
	
	//make sure we have a valid number of arguments
	//if ( argc < 2 ){
	//	printf("Invalid parameters supplied!{ %d }" , argc);
	//	//return 0;
	//}

	pid_t cpid , w;

	cpid = fork();

	if ( cpid == 0 ){
		//call ls
		execve( "/bin/ls" , argv,NULL);
	}else{
		int status;
		while ( !WIFEXITED(status) && !WIFSIGNALED(status)){ 
			w = waitpid( cpid , &status , WUNTRACED | WCONTINUED );
			if ( w==-1){
				exit(0);
			}
			
			if (WIFEXITED(status)) {
				if ( WEXITSTATUS( status ) == 0 ){
					printf("Success.\n");
				}else{
					printf("Failure.\n");
				}
                		//printf("exited, status=%d\n", WEXITSTATUS(status));
            		
			}
		}
	}
	
	return 0;
}

