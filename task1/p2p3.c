#include <stdio.h>
#include <time.h>
#include <unistd.h>

int main(){
	
	//daemonize our proccess
	if ( fork() != 0 )
		return 0;

	while(1){
		time_t timer;
		time(&timer);
		struct tm * timeinfo;
		timeinfo = localtime(&timer);
		
		FILE * file = fopen( "/tmp/lm258.txt" , "a" );
		
		if ( file != 0 ){
			fprintf( file , "The current time is %s" , asctime( timeinfo )  );
			fclose( file );
		}
		
		sleep(10);
	}
	return 0;
}

