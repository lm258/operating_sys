#include "memory.h"

//define our globals to be used in our functions
void * HEAP;
size_t HEAP_SIZE = 0;

//this is our structure to be used for our linked list
struct List {
	struct List * next;
	void * ptr;
	size_t size;
	int free; // If this is 1 then we are free memory
};

//typedef our list struct for easy usage
typedef struct List List;

//our first linked list element
List * lStart = NULL;


/*
 * This is where we will define our linked list memory handler
 *
*/

/*
 * This is our function for initiating our memory
*/
void my_init( size_t heap_size )
{
	//first of all we want to create our heap
	HEAP = sbrk( heap_size );
	HEAP_SIZE = heap_size;

	/* Make sure we have a correct sized heap */
	if ( HEAP == NULL )
		perror("Couldnt get more memory from the OS.\n");

	//create our little linked list handler
	lStart = (List *)malloc( sizeof(List) );
	lStart->next = NULL;
	lStart->size = HEAP_SIZE;
	lStart->ptr = HEAP;
	lStart->free = 1;
}

void *my_malloc( size_t size)
{
	//get first element
	List * current = lStart;

	//go through list
	while ( current != NULL ){

		//check to see if we are big enough and free
		if ( current->size >= size && current->free == 1 ){

			//if we are the same size just return the current item
			if ( current->size == size ){

				//set ourselves not to free and return the pointer
				current->free = 0;
				return current->ptr;
			}

			//if we are then we need to add our element into this list
			List * newList = (List *)malloc( sizeof(List) );

			//seto our size and wether we are free or not
			newList->ptr = ( current->ptr + (current->size - size) );

			//decrement our current size
			current->size -= size;

			newList->next = NULL;
			newList->size = size;
			newList->free = 0;

			//now we want to add the newlist to the position of current
			newList->next = current->next;
			current->next = newList;

			return  newList->ptr;

		}

		current = current->next;
	}

	//return null if we couldnt allocate memory
	return (NULL);
}

void my_free( void *ptr )
{
	//get first element
	List * current = lStart;

	//go through list
	while ( current != NULL ){

		//we have found das ptr
		if ( current->ptr == ptr ){

			//set ourselves to free
			current->free = 1;

			//check if we have free memory next to us , if so merge ourselves
			if ( current->next != NULL && current->next->free == 1 ){

				//add my size to his
				current->size += current->next->size;

				//get his next so i can set it as mines
				List * tmp = current->next->next;

				//free him
				free(current->next);

				//set my next to his next
				current->next = tmp;
			}

			break;
		}

		/* This function is function is used to free our adjacent memory spaces */
		if ( current->next != NULL && current->next->free == 1 && current->free == 1 ){

			//add my size to his
			current->size += current->next->size;

			//get his next so i can set it as mines
			List * tmp = current->next->next;

			//free him
			free(current->next);

			//set my next to his next
			current->next = tmp;
		}

		current = current->next;
	}

}

/*
 * This function is used to free our memory from the heap and also 
 * free up our linked list pointers
 *
*/
void my_exit()
{

	//go through our linked list and free everything
	List * next = lStart;

	//go through the list and free each element
	while ( next != NULL ){
		List * tmp = next->next;
		free(next);
		next = tmp;
	}

  //free our heap when we are done
  sbrk( -HEAP_SIZE );
}
//define our globals to be used in our functions
void * HEAP;
size_t HEAP_SIZE = 0;

//this is our structure to be used for our linked list
struct List {
	struct List * next;
	void * ptr;
	size_t size;
	int free; // If this is 1 then we are free memory
};

//typedef our list struct for easy usage
typedef struct List List;

//our first linked list element
List * lStart = NULL;


/*
 * This is where we will define our linked list memory handler
 *
*/

/*
 * This is our function for initiating our memory
*/
void my_init( size_t heap_size )
{
	//first of all we want to create our heap
	HEAP = sbrk( heap_size );
	HEAP_SIZE = heap_size;

	/* Make sure we have a correct sized heap */
	if ( HEAP == NULL )
		perror("Couldnt get more memory from the OS.\n");

	//create our little linked list handler
	lStart = (List *)malloc( sizeof(List) );
	lStart->next = NULL;
	lStart->size = HEAP_SIZE;
	lStart->ptr = HEAP;
	lStart->free = 1;
}

void *my_malloc( size_t size)
{
	//get first element
	List * current = lStart;

	//go through list
	while ( current != NULL ){

		//check to see if we are big enough and free
		if ( current->size >= size && current->free == 1 ){

			//if we are the same size just return the current item
			if ( current->size == size ){

				//set ourselves not to free and return the pointer
				current->free = 0;
				return current->ptr;
			}

			//if we are then we need to add our element into this list
			List * newList = (List *)malloc( sizeof(List) );

			//seto our size and wether we are free or not
			newList->ptr = ( current->ptr + (current->size - size) );

			//decrement our current size
			current->size -= size;

			newList->next = NULL;
			newList->size = size;
			newList->free = 0;

			//now we want to add the newlist to the position of current
			newList->next = current->next;
			current->next = newList;

			return  newList->ptr;

		}

		current = current->next;
	}

	//return null if we couldnt allocate memory
	return (NULL);
}

void my_free( void *ptr )
{
	//get first element
	List * current = lStart;

	//go through list
	while ( current != NULL ){

		//we have found das ptr
		if ( current->ptr == ptr ){

			//set ourselves to free
			current->free = 1;

			//check if we have free memory next to us , if so merge ourselves
			if ( current->next != NULL && current->next->free == 1 ){

				//add my size to his
				current->size += current->next->size;

				//get his next so i can set it as mines
				List * tmp = current->next->next;

				//free him
				free(current->next);

				//set my next to his next
				current->next = tmp;
			}

			break;
		}

		/* This function is function is used to free our adjacent memory spaces */
		if ( current->next != NULL && current->next->free == 1 && current->free == 1 ){

			//add my size to his
			current->size += current->next->size;

			//get his next so i can set it as mines
			List * tmp = current->next->next;

			//free him
			free(current->next);

			//set my next to his next
			current->next = tmp;
		}

		current = current->next;
	}

}

/*
 * This function is used to free our memory from the heap and also 
 * free up our linked list pointers
 *
*/
void my_exit()
{

	//go through our linked list and free everything
	List * next = lStart;

	//go through the list and free each element
	while ( next != NULL ){
		List * tmp = next->next;
		free(next);
		next = tmp;
	}

  //free our heap when we are done
  sbrk( -HEAP_SIZE );
}