#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "timer.h"
#include "memory.h"

int startsec, startnsec, stopsec, stopnsec;

/*******************************************************************************
 *
 * my allocator begins here!
 *
 *******************************************************************************/
// I have put my memory allocator in the memory.c file

/*******************************************************************************
 *
 * my allocator ends here!
 *
 *******************************************************************************/

void printTimeElapsed( char *text)
{
  double elapsed = (stopsec -startsec)*1000.0
                  + (double)(stopnsec -startnsec)/1000000.0;
  printf( "%s: %f msec\n", text, elapsed);
}


typedef struct LIST {
  char * ptr;
  size_t size;
  struct LIST *next;
} list;

static list *alloc_list = NULL;
static int list_len=0;
static size_t total_allocated=0;

void do_alloc( int c_allocator, int min_alloc, int max_alloc)
{
  int i;
  char *buf;
  list *new_entry;

  size_t size = min_alloc + (rand() % (max_alloc-min_alloc));
  if( c_allocator)
    buf = (char *)malloc( size);
  else 
    buf = (char *)my_malloc( size);

  if( buf==NULL) {
    printf("out of memory\n");
    exit(1);
  } else {
    /* init with value size for consistency check on free*/
    for( i=0; i< (int)size; i++)
      buf[i] = (char) size;

    new_entry = (list*)malloc( sizeof(list));
    new_entry->ptr = buf;
    new_entry->size = size;
    new_entry->next = alloc_list;
    alloc_list = new_entry;
    list_len++;
    total_allocated += size;
  }
}

void do_free_one( int c_allocator)
{
  int i, ok;
  int pick = rand() % list_len;
  list *lptr=alloc_list;
  list *prev=NULL;

  for( i=0; i<pick; i++) {
    prev = lptr;
    lptr = lptr->next;
  }
  
  /* check whether content still intact */
  ok = 1;
  for( i=0; i< (int)lptr->size; i++){
    ok = ok && (lptr->ptr[i] == (char) lptr->size);
  }

  if( !ok) {
    printf( "memory corrupted!\n");
    exit(1);
  }

  if( c_allocator)
    free( lptr->ptr);
  else
    my_free( lptr->ptr);

  if( prev == NULL)
    alloc_list = lptr->next;
  else
    prev->next = lptr->next;

  list_len --;
  total_allocated -= lptr->size;
  free( lptr);
}

void do_free_remaining( int c_allocator)
{
  list *next;

  while( alloc_list != NULL) {
    if( c_allocator)
      free( alloc_list->ptr);
    else
      my_free( alloc_list->ptr);
    list_len --;
    total_allocated -= alloc_list->size;
    next = alloc_list->next;
    free( alloc_list);
    alloc_list = next;
  }
}

int main( int argc, char *argv[])
{
  int i;
  int heap_size = 500000000; // 500 MB 
  int min_alloc = 0;         //   0 MB 
  int max_alloc = 10000;     //  10 KB 
  int num_alloc = 1000;
  int num_alloc_per_free = 3;
  int c_allocator = 0; // default mode uses my_malloc

  if( argc>= 2) {
    heap_size = atoi( argv[1]);
    
    if( argc >= 3) {
      min_alloc = atoi( argv[2]);
      if( min_alloc <= 1) {
        printf( "minimum allocation chosen: %d overwritten to 1\n", min_alloc);
        min_alloc = 1;
      } else if( min_alloc > heap_size) {
        printf( "minimum allocation chosen: %d overwritten to %d\n", min_alloc, heap_size);
        min_alloc = heap_size;
      }

      if( argc>= 4) {
        max_alloc = atoi( argv[3]);
        if( max_alloc <= min_alloc) {
          printf( "maximum allocation chosen: %d overwritten to %d\n", max_alloc, min_alloc);
          max_alloc = min_alloc;
        } else if( max_alloc > heap_size) {
          printf( "maximum allocation chosen: %d overwritten to %d\n", max_alloc, heap_size);
          max_alloc = heap_size;
        }

        if( argc>= 5) {
          num_alloc = atoi( argv[4]);

          if( argc>= 6) {
            num_alloc_per_free = atoi( argv[5]);
            if( num_alloc_per_free < 1) {
              printf( "number of allocations per free chosen as %d overwritten to 1\n", 1);
              num_alloc_per_free = 1;
            }

            if( argc>= 7) {
              if( strcmp( argv[6], "malloc") == 0) {
                printf( "std malloc chosen\n");
                c_allocator = 1;
              } else {
                printf( "unrecognised allocator chosen (\"%s\") using \"my_malloc\"\n", argv[6]);
              }

            }
          }
        }
      }
    }
  }

  // check consistency of preset values
  if( min_alloc > heap_size) {
    min_alloc = heap_size;
  }
  if( max_alloc > heap_size) {
    max_alloc = heap_size;
  }

  // print setup
  printf( "\n--------------- Setup ------------------\n");
  if( heap_size <1000000)
    printf( "trying to use %d B heap\n", heap_size);
  else
    printf( "trying to use %4.0f MB heap\n", heap_size/1000000.0);

  if( min_alloc <1000000)
    printf( "minimum allocation set to %d B\n", min_alloc);
  else
    printf( "minimum allocation set to %4.0f MB\n", min_alloc/1000000.0);

  if( max_alloc <1000000)
    printf( "maximum allocation set to %d B\n", max_alloc);
  else
    printf( "maximum allocation set to %4.0f MB\n", max_alloc/1000000.0);

  printf( "number of random allocations set to %d\n", num_alloc);
  printf( "number of allocations per free set to %d\n", num_alloc_per_free);
  if( c_allocator)
    printf( "using \"malloc\"\n");
  else
    printf( "using \"my_malloc\"\n");
  printf( "----------------------------------------\n");

  if( !c_allocator)
    my_init( heap_size);
  srand( time(NULL));

  TIMERwc_time( &startsec, &startnsec);

  for( i =0; i < num_alloc ; i ++) {
    do_alloc( c_allocator, min_alloc, max_alloc);

    if( i % num_alloc_per_free == 0) {
      do_free_one( c_allocator );
    }
    if( i % 1000 == 0) {
      printf(" %d allocations done....current allocation: %d\n", i, (int)total_allocated);
    }
  }
  do_free_remaining( c_allocator );

  TIMERwc_time( &stopsec, &stopnsec);

  printTimeElapsed( "wall clock time spent");

  if( !c_allocator)
    my_exit();

  return 0;
} 
