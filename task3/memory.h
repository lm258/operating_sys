#ifndef MEMORY_H
#define MEMORY_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "timer.h"
#include <unistd.h>

void my_init( size_t heap_size);

void *my_malloc( size_t size);

void my_free( void *ptr);

void my_exit();

#endif