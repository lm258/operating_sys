#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>

void * dummy (void * arg)
{
   return arg;
}

double xelapsed (struct timespec a, struct timespec b)
{
   return (a.tv_sec - b.tv_sec) * 1000000.0
          + (a.tv_nsec - b.tv_nsec) / 1000.0;
}

void measure_thread (int n)
{
   struct timespec start, stop, finish;
   pthread_t child_pids[n];
   
   clock_gettime (CLOCK_REALTIME, &start);
   /*
    * Make N forks, call dummy in every child.
    */
    for( int i = 0 ; i < n ; i++ ){
        
      if ( pthread_create (&child_pids[i], NULL, dummy, NULL) != 0 ){
            child_pids[i] = 0;

            //this is used just incase we cant create a thread
            fprintf( stderr , "ERROR: Could not create a thread\n");
            exit(1);
      }
    }

   clock_gettime (CLOCK_REALTIME, &stop);
   /*
    * Wait for the forks.
    */
    for( int i = 0 ; i < n ; i++ ){
        void *result;
        
        if ( child_pids[i] != 0 )
            pthread_join (child_pids[i], &result );
    }

   clock_gettime (CLOCK_REALTIME, &finish);

   printf ("%d proc: thread=%5.0f (mics) wait=%5.0f (mics) sum=%5.0f (mics)\n",
           n, xelapsed (stop, start),
           xelapsed (finish, stop), xelapsed (finish, start));
   printf ("per proc: thread=%5.0f (mics) wait=%5.0f (mics) sum=%5.0f (mics)\n",
           xelapsed (stop, start)/n,
           xelapsed (finish, stop)/n, xelapsed (finish, start)/n);
}

int main(int argc, char*argv[])
{
   int n;

   if( argc == 2) {
     n = atoi( argv[1]);
  } else {
     n = 10;
  }
  measure_thread( n);

  return(0);
}
