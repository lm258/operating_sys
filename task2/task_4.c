#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>

//this is the number that we have produced
int producedNumber = 0;
int consumedNumber = 0;
int producerLimit = 0;
pthread_mutex_t lock;
pthread_cond_t changedValue = PTHREAD_COND_INITIALIZER;

//this is our runnig variable
int running = 1;

void * producer(){
	
	while ( 1 ){

		//lock our mutex
		pthread_mutex_lock(&lock);

		//make sure the we can produce a new number ( only if it has been consumed first )
		if ( producedNumber == consumedNumber )
			producedNumber += 1;

		//check to see if we are ready to exit or not
		if ( producedNumber > producerLimit && consumedNumber > producerLimit ){
			pthread_cond_signal(&changedValue);

			//unlock the counter
			pthread_mutex_unlock(&lock);

			break;
		}

		pthread_cond_signal(&changedValue);

		//unlock the counter
		pthread_mutex_unlock(&lock);
	}

	return NULL;
}

void * consumer( void * id ){

	int ID = (int)id;

	while( 1 ){

		pthread_mutex_lock(&lock);

		//wait for our condition to say that the number has been changed
		pthread_cond_wait(&changedValue, &lock);

		if ( consumedNumber > producerLimit ){
			pthread_mutex_unlock(&lock);
			break;
		}

		//make sure the cosnumed number is smaller than the produced number
		if ( consumedNumber < producedNumber ){
			printf( "ID:%d : Consumed Int %d\n" , ID , consumedNumber );
			consumedNumber++;
		}

		pthread_mutex_unlock(&lock);
	}

	return NULL;
}


int main ( int argc , char ** argv )
{
	if ( argc != 2 ){
		fprintf( stderr , "Please supplly correct arguments to this program. Arg1 should be number of numbers to produce.\n" );
		exit(0);
	}

	//try and initate our mutex lock 
	if ( pthread_mutex_init(&lock, NULL ) != 0 )
    {
        fprintf( stderr , "Error: Mutex init failed\n");
        exit(0);
    }

	//create our empty pthread containers
	pthread_t producer_id;
	pthread_t consumer1;
	pthread_t consumer2;

	//convert our nice little first argument into an integer
	producerLimit = atoi( argv[1] );

	//create our pthread
	pthread_create (&producer_id, NULL, producer, NULL );
	pthread_create (&consumer1, NULL, consumer, (void*)0 );
	pthread_create (&consumer2, NULL, consumer, (void*)1 );

	//join our threads
	pthread_join( consumer2, NULL);
	pthread_join( consumer1, NULL);
	pthread_join( producer_id, NULL);
	pthread_mutex_destroy(&lock);
}